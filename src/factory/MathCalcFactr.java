package factory;
//Factory
public class MathCalcFactr {
    public MathCalc doMathCalc(String math){
        if(math.equalsIgnoreCase("SumCl")){
            return new SumCl();
        }
        else if(math.equalsIgnoreCase("Divide")){
            return new Divide();
        }
        else return null;
    }
}
