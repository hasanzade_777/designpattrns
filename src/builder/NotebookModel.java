package builder;

import java.util.ArrayList;
import java.util.List;
public class NotebookModel {
    private List<Pack> items=new ArrayList<Pack>();
    public void addItem(Pack pack){
        items.add(pack);
    }
    public void getItems(){
        for(Pack pack : items){
            System.out.print("Notebook Model:"+pack.pack());
            System.out.print(" Price:"+pack.price());
            System.out.print(" Color:"+pack.color());
            System.out.println(" CPU:"+pack.cpu());
        }
    }
}
