package builder;

public class NotebookBuilder {
    NotebookModel model=new NotebookModel();
    public NotebookModel buildLenovo(){
        model.addItem(new Lenoovo());
        return model;
    }
    NotebookModel model2=new NotebookModel();
    public NotebookModel buildDell(){
        model2.addItem(new Dell());
        return model2;
    }

}
