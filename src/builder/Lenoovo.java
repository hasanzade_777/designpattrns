package builder;

public class Lenoovo extends Company{
    @Override
    public int price() {
        return 150;
    }

    @Override
    public String color() {
        return "RED";
    }

    @Override
    public String pack() {
        return "Lenovo Notebook";
    }

    @Override
    public String cpu() {
        return "RYZEN";
    }
}
