package builder;

public abstract class Company extends Notebook {
    public abstract int price();
    public abstract String color();
}
