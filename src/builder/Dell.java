package builder;

public class Dell extends Notebook{
    @Override
    public String pack() {
        return "DELL Notebook";
    }

    @Override
    public int price() {
        return 250;
    }

    @Override
    public String cpu() {
        return "INTEL";
    }

    @Override
    public String color() {
        return "Black";
    }
}
