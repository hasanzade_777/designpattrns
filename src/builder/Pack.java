package builder;
//Builder Class
public interface Pack {
    String pack();
    int price();
    String cpu();
    String color();
}
