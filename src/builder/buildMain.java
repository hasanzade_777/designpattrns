package builder;

public class buildMain {
    public static void main(String[] args) {
        NotebookBuilder builder = new NotebookBuilder();
        NotebookModel model1 = builder.buildLenovo();
        model1.getItems();
        NotebookModel model2 = builder.buildDell();
        model2.getItems();
    }
}
