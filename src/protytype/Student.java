package protytype;

public class Student implements Prototip{
    private int id;
    private String name;
    private String surname;
    public Student(int id,String name,String surname){
        this.id=id;
        this.name=name;
        this.surname=surname;
    }
    void show(){
        System.out.println("ID:"+id+"\n name:"+name+"\n surname:"+surname);
    }
    @Override
    public Prototip getClone() {
        return new Student(id,name,surname);
    }

    public static void main(String[] args) {
        Student student=new Student(1,"Elbrus","Hasanzada");
        Student student1=(Student)student.getClone();
        student.show();
        System.out.println("Prototip:");
        student1.show();
    }
}
