package protytype;

public interface Prototip {
    public Prototip getClone();
}
