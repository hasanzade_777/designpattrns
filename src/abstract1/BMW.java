package abstract1;
//Abstract Method
public class BMW extends Car{
    private String Car="BMW";
    private String Model;
    private int Price;
    private String Color;
    public BMW(String Model,int Price,String Color){
        this.Car=Car;
        this.Model=Model;
        this.Price=Price;
        this.Color=Color;
    }

    @Override
    public String getCar() {
        return this.Car=Car;
    }

    @Override
    public String getModel() {
        return this.Model;
    }

    @Override
    public int getPrice() {
        return this.Price;
    }

    @Override
    public String getColor() {
        return this.Color;
    }
}
