package abstract1;
//Abstract Method
public abstract class Car{
    public abstract String getCar();
    public abstract String getModel();
    public abstract int getPrice();
    public abstract String getColor();

    @Override
    public String toString() {
        return "Car:"+this.getCar()+" Model:"+this.getModel()+" Price:"+this.getPrice()+" Color:"+this.getColor();
    }
}
