package singleton;
//Singleton
public final class Animal {
    private static Animal Instance;
    public int n;
    public static Animal getInstance(int n){
        if(Instance==null){
            Instance=new Animal(n);
        }
        return Instance;
    }
    private Animal(int n){
        this.n=n;
    }
}
